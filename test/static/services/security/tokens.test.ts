import jwt from 'jsonwebtoken'
import * as tokens from '../../../../src/services/security/tokens'

describe('Services security', () => {

  // beforeAll(async () => {
  // })

  // afterAll(async () => {
  // })

  test('Create access token payload', async () => {
    
    const userId = 123
    const email = 'test@test.com'
    const profileId = 12

    const shouldReturn = {i: userId, e: email, p: profileId, r: 10}

    const returned = await tokens.createAccessTokenPayload(userId, email, profileId, 10)
    expect(returned).toEqual(shouldReturn)
  })

  test('Create refresh token payload', async () => {
    
    const userId = 123

    const shouldReturn = {i: userId}

    const returned = await tokens.createRefreshTokenPayload(userId)
    expect(returned).toEqual(shouldReturn)
  })

  test('Create Token', async () => {
    
    const payload = {hello: 'hi'}
    const secret = 'SuperSecret!'

    const returned = await tokens.createToken(payload, secret, '10m')
    
    expect(returned).toBeDefined()

    const validatePayload: any = jwt.verify(returned, secret)
    expect(validatePayload?.hello).toEqual(payload.hello)
  })

  test('Validate Token', async () => {
    const payload = {hello: 'hi'}
    const secret = 'SuperSecret!'

    const token = jwt.sign(payload, secret, {algorithm: 'HS384'})

    const validatedToken = await tokens.validateToken(token, secret)

    expect(validatedToken?.hello).toEqual(payload.hello)
  
  })

})