import * as hashing from '../../../../src/services/security/hashing'

describe('Services security', () => {

  // beforeAll(async () => {
  // })

  // afterAll(async () => {
  // })

  test('Hash password', async () => {
    
    const passwordToHash = "test123456789"

    const hashedPassword = await hashing.hashPassword(passwordToHash)

    expect(hashedPassword === passwordToHash).toEqual(false)
    expect(hashedPassword).toBeDefined()
  })

  test('Verify hashed password', async () => {
    
    const passwordToHash = "test123456789"
    const badPasswordToCheck = "000"

    const hashedPassword = await hashing.hashPassword(passwordToHash)

    const verifyTruePassword = await hashing.validatePassword(hashedPassword, passwordToHash)
    const verifyFalsePassword = await hashing.validatePassword(hashedPassword, badPasswordToCheck)

    expect(verifyTruePassword).toEqual(true)
    expect(verifyFalsePassword).toEqual(false)
  })
})