import { refreshTokenKey } from '../../../../src/services/helpers/redis'

describe('Services helpers', () => {

  // beforeAll(async () => {
  // })

  // afterAll(async () => {
  // })

  test('Get refresh token key', async () => {
    
    const id = 123
    const shouldBe = "rtk_123"

    const rtk = await refreshTokenKey(id)

    expect(rtk).toEqual(shouldBe)
  })
})