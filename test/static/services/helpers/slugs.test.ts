import { slugify } from '../../../../src/services/helpers/slugs'

describe('Services helpers', () => {

  // beforeAll(async () => {
  // })

  // afterAll(async () => {
  // })

  test('Slugify', async () => {
    
    const toSlugify = "Tomą tókr"
    const shouldBe = "toma-tokr"

    const afterSlugify = await slugify(toSlugify)

    expect(afterSlugify).toEqual(shouldBe)
  })
})