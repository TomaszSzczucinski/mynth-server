import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Sign In', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Login to user account', async () => {
    const user = await usersTester.createOne()

    const res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.addTokens(res.headers['set-cookie'])
    
    expect(user.refreshToken).toBeDefined()
    expect(user.accessToken).toBeDefined()

  })

  test('Refresh Access Token', async () => {
    const user = await usersTester.createOne()

    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    let response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.addTokens(res.headers['set-cookie'])
    
    expect(user.refreshToken).toBeDefined()
    expect(user.accessToken).toBeDefined()

    res = await server.inject({
      method: 'GET',
      url: '/refreshToken',
      headers: {
        'cookie': `rt=${user.refreshToken}`
      }
    })

    response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    res = await server.inject({
      method: 'GET',
      url: '/refreshToken',
      headers: {
        'cookie': `at=${user.refreshToken}`
      }
    })

    expect(res.statusCode).toEqual(401)

    res = await server.inject({
      method: 'GET',
      url: '/refreshToken'
    })

    expect(res.statusCode).toEqual(401)

  })

})