import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Update Account', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Set new password', async () => {
    const user = await usersTester.createOne()
    const oldPassword = user.plainPassword    

    const isLoggedIn = await user.login(server)

    expect(isLoggedIn).toBeTruthy()

    await user.generateNewPassword()

    expect(oldPassword === user.plainPassword).toBeFalsy()

    const res = await server.inject({
      method: 'POST',
      url: '/newPassword',
      payload: {
        password: oldPassword,
        newPassword: user.plainPassword
      },
      headers: {
        'cookie': `at=${user.accessToken}`
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.update()

    const isValid = await validatePassword(user.user.password, user.plainPassword)

    expect(isValid).toBeTruthy()
  })

  test('Set new email', async () => {
    const user = await usersTester.createOne()
    const newEmail = 'test@mynth.io'

    await user.update()
    const isLoggedIn = await user.login(server)

    expect(isLoggedIn).toBeTruthy()

    const res = await server.inject({
      method: 'POST',
      url: '/newEmail',
      payload: {
        password: user.plainPassword,
        email: newEmail,
      },
      headers: {
        'cookie': `at=${user.accessToken}`
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.updateById()

    expect(user.user.email).toEqual(newEmail)
  })

  test('Set new email (Bad password scenario)', async () => {
    const user = await usersTester.createOne()
    const newEmail = 'test@mynth.io'

    await user.update()
    const isLoggedIn = await user.login(server)

    expect(isLoggedIn).toBeTruthy()

    const res = await server.inject({
      method: 'POST',
      url: '/newEmail',
      payload: {
        password: 'Thispasswordisbadforsure',
        email: newEmail,
      },
      headers: {
        'cookie': `at=${user.accessToken}`
      }
    })

    expect(res.statusCode).toEqual(401)
  })
})
