import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Reset Password', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Create token and valid to date', async () => {
    const user = await usersTester.createOne()

    const res = await server.inject({
      method: 'POST',
      url: '/resetPassword',
      payload: {
        email: user.user.email,
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.update()

    expect(user && user.user.reset_token_valid_to).toBeDefined()

    let validTo = dayjs('1999-01-01')
    if (user.user.reset_token_valid_to) validTo = dayjs(user.user.reset_token_valid_to)

    expect(dayjs().isBefore(validTo)).toBeTruthy()
    expect(dayjs(dayjs().add(20, 'minutes')).isAfter(validTo)).toBeTruthy()
  })

  test('Proper error message if email not exists', async () => {
    const testEmail = 'doesnotexistsforsure@mynth.ioio'
    const res = await server.inject({
      method: 'POST',
      url: '/resetPassword',
      payload: {
        email: testEmail,
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(400)
    expect(response.message).toBeDefined()

  })

  test('Reset password using token', async () => {
    const user = await usersTester.createOne()

    const res = await server.inject({
      method: 'POST',
      url: '/resetPassword',
      payload: {
        email: user.user.email,
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.update()

    expect(user.user.reset_token).toBeDefined()

    await user.generateNewPassword()

    // Inject
    const res2 = await server.inject({
      method: 'POST',
      url: '/resetPassword/' + user.user.reset_token,
      payload: {
        password: user.plainPassword,
      }
    })

    const response2 = JSON.parse(res2.payload)
    expect(res2.statusCode).toEqual(200)
    expect(response2.success).toEqual(true)

    await user.update()

    const isValid = await validatePassword(user.user.password, user.plainPassword)

    expect(isValid).toBeTruthy()
  })

})
