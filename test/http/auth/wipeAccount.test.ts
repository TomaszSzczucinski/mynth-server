import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Wipe Account', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Wipe account', async () => {
    const user = await usersTester.createOne()

    const isLoggedIn = await user.login(server)

    expect(isLoggedIn).toBeTruthy()

    const res = await server.inject({
      method: 'POST',
      url: '/wipeAccount',
      payload: {
        password: user.plainPassword
      },
      headers: {
        'cookie': `at=${user.accessToken}`
      }
    })

    const response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    expect(await user.update()).toBeFalsy()
  })

})
