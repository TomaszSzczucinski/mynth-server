import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'

describe('Sign up', () => {
  let server: Hapi.Server

  beforeAll(async () => {
    server = await createServer()
  })

  afterAll(async () => {
    const { prisma } = server.app

    await prisma.profile.deleteMany({
      where: {
        name: {
          startsWith: 'testTestName'
        }
      }
    })

    await prisma.user.deleteMany({
      where: {
        name: {
          startsWith: 'testTestName'
        }
      }
    })
    await server.stop()
  })

  test('Email taken validate', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName123',
        email: 'test123456@test.com',
        password: 'test123456'
      }
    })

    expect(res.statusCode).toEqual(200)

    const response = JSON.parse(res.payload)
    expect(response.success).toEqual(true)

    const res2 = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName1234',
        email: 'test123456@test.com',
        password: 'test123456'
      }
    })

    expect(res2.statusCode).toEqual(400)
  })

  test('Properly create user', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName12345',
        email: 'test123@test.com',
        password: 'test123456'
      }
    })

    expect(res.statusCode).toEqual(200)

    const response = JSON.parse(res.payload)
    expect(response.success).toEqual(true)
  })

  test('Create user - Validation test', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName123456',
        email: 'test123',
        password: 'test123456'
      }
    })

    expect(res.statusCode).toEqual(400)
  })

  test('Name mynth not allowed', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'mynth',
        email: 'test123@test.com',
        password: 'test123456'
      }
    })

    expect(res.statusCode).toEqual(400)
  })

  test('Create user - Validation test 2', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        email: 'test123@gmail.com',
        password: 'test123456'
      }
    })

    expect(res.statusCode).toEqual(400)
  })

  test('Create user - Validation test 3', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName123456',
        email: 'test123@gmail.com',
        password: 't6'
      }
    })

    expect(res.statusCode).toEqual(400)
  })

  test('Create user - Validation test 4', async () => {
    const res = await server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: 'testTestName123456',
        email: 'test123@gmail.com'
      }
    })

    expect(res.statusCode).toEqual(400)
  })
})
