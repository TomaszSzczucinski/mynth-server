import Hapi from '@hapi/hapi'
import { PrismaClient, User } from '@prisma/client'
import axios from 'axios'
import {nanoid} from 'nanoid/async'

class TestUser {
  private prisma: PrismaClient
  user: User
  accessToken: string | undefined
  refreshToken: string | undefined
  plainPassword: string

  constructor(name: string, email: string, password: string, prisma: PrismaClient) {
    this.prisma = prisma
    this.plainPassword = password
    this.accessToken = undefined
    this.refreshToken = undefined
    this.user = {
      id: 0,
      name: name,
      email: email,
      password: "",
      role: 10,
      is_blocked: false,
      reset_token: null,
      reset_token_valid_to: null
    }
  }

  async getSignUpData(): Promise<{
    name: string
    email: string
    password: string
  }> {
    return {
      name: this.user.name,
      email: this.user.email,
      password: this.user.password
    }
  }

  public async addTokens(setCookie: string[] | string): Promise<void> {
    if (!Array.isArray(setCookie)) setCookie = [setCookie]
    for (let i = 0; i < setCookie.length; i++)
    {
      const cookie = setCookie[i]
      if (cookie.startsWith('a'))
      {
        this.accessToken = cookie.substr(3, cookie.length).split(';')[0]
      }
      else if (cookie.startsWith('r'))
      {
        this.refreshToken = cookie.substr(3, cookie.length).split(';')[0]
      }
    }
  }

  public async login(server: Hapi.Server): Promise<boolean> {
    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: this.user.email,
        password: this.plainPassword
      }
    })

    if (res.statusCode !== 200) return false

    await this.addTokens(res.headers['set-cookie'])

    return res.statusCode === 200
  }

  public async generateNewPassword():Promise<void> {
    const newPassword = await nanoid(8)
    this.plainPassword = newPassword
  }

  public async update(): Promise<boolean> {
    const userData = await this.prisma.user.findUnique({
      where: {
        email: this.user.email
      }
    })

    if (userData)
    {
      this.user = userData
      return true
    }
    return false
  }

  public async updateById(): Promise<void> {
    const userData = await this.prisma.user.findUnique({
      where: {
        id: this.user.id
      }
    })

    if (userData)
    {
      this.user = userData
    }
  }
}

class UsersTester {
  private server: Hapi.Server
  private prisma: PrismaClient
  private usersList: TestUser[]

  constructor(server: Hapi.Server, prisma: PrismaClient) {
    this.server = server
    this.prisma = prisma
    this.usersList = []
  }

  /**
   * Create user with random data
   */
  public async createOne(): Promise<TestUser> {
    const user = await this.generateUser()
    await user.generateNewPassword()
    const res = await this.server.inject({
      method: 'POST',
      url: '/signup',
      payload: {
        name: user.user.name,
        email: user.user.email,
        password: user.plainPassword
      }
    })

    if (res.statusCode !== 200) throw Error('Cannot signup')
    this.usersList.push(user)
    return user
  }

  /**
   * Generate user data
   */
  private async generateUser(): Promise<TestUser> {
    const res = await axios.get('https://randomuser.me/api/')
    if (res.status !== 200) throw new Error('No data')

    const data = res.data.results[0]

    if (!data) throw new Error('No data')

    return new TestUser(
      data.name.first + ' ' + data.name.last,
      data.email,
      data.login.password,
      this.prisma
    )
  }

  public async clearAccounts(): Promise<void> {
    for (let i = 0; i < this.usersList.length; i++) {
      const user = this.usersList[i]
      await user.update()
      await this.wipeUser(user)
    }
  }

  public async wipeUser(user: TestUser): Promise<void> {
    await this.prisma.postDraft.deleteMany({
      where: {
        profileId: user.user.id
      }
    })

    await this.prisma.post.deleteMany({
      where: {
        profileId: user.user.id
      }
    })

    await this.prisma.postLike.deleteMany({
      where: {
        profileId: user.user.id
      }
    })

    await this.prisma.profile.deleteMany({
      where: {
        user: {
          email: user.user.email
        }
      }
    }).then(async () => {
      await this.prisma.user.delete({
        where: {
          email: user.user.email
        }
      })
    })
  }
}

export default UsersTester