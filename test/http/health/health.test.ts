import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'

describe('Health', () => {
  let server: Hapi.Server

  beforeAll(async () => {
    server = await createServer()
  })

  afterAll(async () => {
    await server.stop()
  })

  test('Health endpoint returns 200 and up: true', async () => {
    const res = await server.inject({
      method: 'GET',
      url: '/'
    })

    expect(res.statusCode).toEqual(200)

    const response = JSON.parse(res.payload)
    expect(response.up).toEqual(true)
  })
})