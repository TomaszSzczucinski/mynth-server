import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import fs from 'fs'
import dayjs from 'dayjs'
import FormData from 'form-data'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Profile', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Change Avatar', async () => {
    const user = await usersTester.createOne()

    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    await user.addTokens(res.headers['set-cookie'])

    let fd = new FormData()
    const fileAvatar = fs.readFileSync('./test/resources/avatar.jpg')
    fd.append('avatar', fileAvatar, 'avatar.jpg')

    const fdHeaders = fd.getHeaders()

    res = await server.inject({
      method: 'POST',
      url: '/avatar',
      payload: fd.getBuffer(),
      headers: {
        cookie: `at=${user.accessToken}`,
        ...fdHeaders
      }
    })

    const response = JSON.parse(res.payload)

    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)
  })

  test('Get Avatar (null)', async () => {
    const user = await usersTester.createOne()

    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    await user.addTokens(res.headers['set-cookie'])

    res = await server.inject({
      method: 'GET',
      url: '/avatar',
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    const response = JSON.parse(res.payload)

    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)
    expect(response.avatar).toBe(null)
  })

  test('Change Avatar and Get avatar', async () => {
    jest.setTimeout(10000)

    const user = await usersTester.createOne()

    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    await user.addTokens(res.headers['set-cookie'])

    let fd = new FormData()
    const fileAvatar = fs.readFileSync('./test/resources/avatar.jpg')
    fd.append('avatar', fileAvatar, 'avatar.jpg')

    const fdHeaders = fd.getHeaders()

    res = await server.inject({
      method: 'POST',
      url: '/avatar',
      payload: fd.getBuffer(),
      headers: {
        cookie: `at=${user.accessToken}`,
        ...fdHeaders
      }
    })

    let response = JSON.parse(res.payload)

    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await new Promise((r) => setTimeout(r, 6000))

    res = await server.inject({
      method: 'GET',
      url: '/avatar',
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    response = JSON.parse(res.payload)

    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)
    expect(typeof response.avatar).toBe('string')
    expect(response.avatar.startsWith('http')).toEqual(true)
  })
})
