import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('Edit post', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Edit post', async () => {
    const user = await usersTester.createOne()
    await user.login(server)

    const r = await server.inject({
      method: 'POST',
      url: '/post',
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    expect(r.statusCode).toEqual(200)

    const darftId = JSON.parse(r.payload).post
    const payload = {
      data: {
        title: 'Awesome title',
        blocks: [
          {
            type: 'text',
            content: 'Hello World!'
          }
        ]
      }
    }

    const r2 = await server.inject({
      method: 'PUT',
      url: '/post/' + darftId,
      payload: payload,
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    expect(r2.statusCode).toEqual(200)
  })

  test('Edit post - fail unauth', async () => {
    const user = await usersTester.createOne()
    const user2 = await usersTester.createOne()

    await user.login(server)
    await user2.login(server)

    const r = await server.inject({
      method: 'POST',
      url: '/post',
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    expect(r.statusCode).toEqual(200)

    // 1st user draft ID
    const darftId = JSON.parse(r.payload).post

    const payload = {
      data: {
        title: 'Awesome title',
        blocks: [
          {
            type: 'text',
            content: 'Hello World!'
          }
        ]
      }
    }

    const r2 = await server.inject({
      method: 'PUT',
      url: '/post/' + darftId,
      payload: payload,
      headers: {
        cookie: `at=${user2.accessToken}`
      }
    })

    expect(r2.statusCode).toEqual(500)
  })
})
