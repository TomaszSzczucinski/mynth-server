import Hapi from '@hapi/hapi'
import { createServer } from '../../../src/server'
import UsersTester from '../helpers/account'
import dayjs from 'dayjs'
import { validatePassword } from '../../../src/services/security/hashing'

describe('New post', () => {
  let server: Hapi.Server
  let usersTester: UsersTester

  beforeAll(async () => {
    server = await createServer()
    const { prisma } = server.app
    usersTester = new UsersTester(server, prisma)
  })

  afterAll(async () => {
    await usersTester.clearAccounts()
    await server.stop()
  })

  test('Create new post', async () => {
    const user = await usersTester.createOne()

    let res = await server.inject({
      method: 'POST',
      url: '/signin',
      payload: {
        email: user.user.email,
        password: user.plainPassword
      }
    })

    let response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)

    await user.addTokens(res.headers['set-cookie'])

    expect(user.refreshToken).toBeDefined()
    expect(user.accessToken).toBeDefined()

    res = await server.inject({
      method: 'POST',
      url: '/post',
      headers: {
        cookie: `at=${user.accessToken}`
      }
    })

    response = JSON.parse(res.payload)
    expect(res.statusCode).toEqual(200)
    expect(response.success).toEqual(true)
    expect(typeof response.post).toBe('number')
  })
})
