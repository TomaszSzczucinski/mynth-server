import Redis from 'ioredis'
import Hapi from '@hapi/hapi'

declare module '@hapi/hapi' {
  interface ServerApplicationState {
    redis: Redis.Redis
  }
}

/**
 * Redis hapi plugin
 */
const redisPlugin = {
  name: 'redis',
  register: async function (server: Hapi.Server): Promise<void> {
    const redis = new Redis({
      port: Number(process.env.REDIS_PORT),
      password: process.env.REDIS_PASS,
      db: Number(process.env.REDIS_DBID)
    })

    server.app.redis = redis

    server.ext({
      type: 'onPostStop',
      method: async (server: Hapi.Server) => {
        server.app.redis.quit()
      }
    })
  }
}

export default redisPlugin
