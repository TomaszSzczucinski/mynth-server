import prismaPlugin from './prisma'
import redisPlugin from './redis'

export default [prismaPlugin, redisPlugin]
