import { PrismaClient } from '@prisma/client'
import Hapi from '@hapi/hapi'

declare module '@hapi/hapi' {
  interface ServerApplicationState {
    prisma: PrismaClient
  }
}

/**
 * Prisma hapi plugin
 */
const prismaPlugin = {
  name: 'prisma',
  register: async function (server: Hapi.Server): Promise<void> {
    const prisma = new PrismaClient()
    server.app.prisma = prisma
    server.ext({
      type: 'onPostStop',
      method: async (server: Hapi.Server) => {
        server.app.prisma.$disconnect()
      }
    })
  }
}

export default prismaPlugin
