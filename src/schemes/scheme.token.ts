import Hapi from '@hapi/hapi'
import Boom from '@hapi/boom'
import dotenv from 'dotenv'
import { validateToken } from '../services/security/tokens'

dotenv.config()

declare module '@hapi/hapi' {
  interface AuthCredentials {
    userId: number
    email: string
    profileId: number
    role: number
  }
}

export default function (
  server: Hapi.Server,
  options: any
) {
  return {
    authenticate: function (request: Hapi.Request, h: Hapi.ResponseToolkit) {
      if (!process.env.ACCESS_SECRET) throw Boom.internal()

      // Get Access Token from cookie
      const token = request.state.at

      // Check if it's there
      if (token) {
        // Validating token
        return validateToken(token, process.env.ACCESS_SECRET)
          .then((payload: { i: number; e: string; p: number; r: number }) => {
            // If validated and payload
            if (payload && payload.r === options.role || payload.r === 1000)
              return h.authenticated({
                credentials: {
                  userId: payload.i,
                  email: payload.e,
                  profileId: payload.p,
                  role: payload.r
                }
              })
            else return Boom.unauthorized()
          })
          .catch(() => {
            return Boom.unauthorized()
          })
      }

      // Unauthorized, token is not present or it's unvalid
      return Boom.unauthorized('Unauthorized')
    }
  }
}
