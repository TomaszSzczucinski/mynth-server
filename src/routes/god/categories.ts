import { Request, ResponseToolkit } from '@hapi/hapi'
import Joi from 'joi'

// Mynth
export default [
  {
    method: 'GET',
    path: '/g/categories',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const categories = await prisma.category.findMany()

      return { success: true, categories: categories }
    },
    options: {
      auth: 'god'
    }
  },
  {
    method: 'POST',
    path: '/g/categories',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const { name, description } = request.payload as {
        name: string
        description: string
      }

      const category = await prisma.category.create({
        data: {
          name: name,
          description: description
        }
      })

      return { success: true, category: category }
    },
    options: {
      auth: 'god',
      validate: {
        payload: Joi.object({
          name: Joi.string().required().min(3),
          description: Joi.string().required().min(3)
        })
      }
    }
  },
  {
    method: 'DELETE',
    path: '/g/categories',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const { ids } = request.payload as { ids: number[] }

      const deleted = await prisma.category.deleteMany({
        where: {
          id: {
            in: ids
          }
        }
      })

      return { success: true, deleted: deleted }
    },
    options: {
      auth: 'god',
      validate: {
        payload: Joi.object({
          ids: Joi.array().items(Joi.number()).required()
        })
      }
    }
  }
]
