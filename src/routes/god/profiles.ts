import { Request, ResponseToolkit } from '@hapi/hapi'
import Joi from 'joi'

// Mynth
export default [
  {
    method: 'GET',
    path: '/g/profiles/{id?}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const profileId = request.params.id ? Number(request.params.id) : null

      const query = profileId ? prisma.profile.findMany({where: {id: profileId}}) : prisma.profile.findMany()

      const profiles = await query

      return { success: true, profiles: profiles }
    },
    options: {
      auth: 'god',
      validate: {
        params: Joi.object({
          id: Joi.number().min(0)
        })
      }
    }
  }
]
