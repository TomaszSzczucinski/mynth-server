import categories from './categories'
import technologies from './technologies'
import profiles from './profiles'

export default [
  ...categories,
  ...technologies,
  ...profiles
]
