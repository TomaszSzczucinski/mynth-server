import { Request, ResponseToolkit } from '@hapi/hapi'
import Joi from 'joi'

// Mynth
export default [
  {
    method: 'GET',
    path: '/g/technologies',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const technologies = await prisma.technology.findMany()

      return { success: true, technologies: technologies }
    },
    options: {
      auth: 'god'
    }
  },
  {
    method: 'POST',
    path: '/g/technologies',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const { name, description } = request.payload as {
        name: string
        description: string
      }

      const technology = await prisma.technology.create({
        data: {
          name: name,
          description: description
        }
      })

      return { success: true, technology: technology }
    },
    options: {
      auth: 'god',
      validate: {
        payload: Joi.object({
          name: Joi.string().required().min(3),
          description: Joi.string().required().min(3)
        })
      }
    }
  },
  {
    method: 'DELETE',
    path: '/g/technologies',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get prisma client
      const { prisma } = request.server.app

      const { ids } = request.payload as { ids: number[] }

      const deleted = await prisma.technology.deleteMany({
        where: {
          id: {
            in: ids
          }
        }
      })

      return { success: true, deleted: deleted }
    },
    options: {
      auth: 'god',
      validate: {
        payload: Joi.object({
          ids: Joi.array().items(Joi.number()).required()
        })
      }
    }
  }
]
