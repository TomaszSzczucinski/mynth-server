import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import dotenv from 'dotenv'
// Mynth
import {
  createAccessTokenPayload,
  createToken,
  validateToken
} from '../../services/security/tokens'

dotenv.config()

export default {
  method: 'GET',
  path: '/refreshToken',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Make sure we have our secret available
    if (
      !process.env.REFRESH_SECRET ||
      !process.env.REFRESH_LIFE ||
      !process.env.ACCESS_SECRET ||
      !process.env.ACCESS_LIFE
    )
      return Boom.internal()

    // Extract token from request cookie
    const refreshToken = request.state.rt

    if (refreshToken) {
      const payload = await validateToken(
        refreshToken,
        process.env.REFRESH_SECRET
      ).catch(() => Boom.unauthorized())

      // Get prisma client
      const { prisma } = request.server.app

      // User ID from payload
      const userId = payload.i
      const user = await prisma.user.findUnique({
        where: {
          id: userId
        },
        include: {
          profile: {
            select: {
              id: true
            }
          }
        }
      })

      // If user isn't blocked
      if (user && !user.is_blocked) {
        // Create access token
        const accessPayload = await createAccessTokenPayload(
          user.id,
          user.email,
          user?.profile?.id || 0,
          user.role
        )
        const accessToken = await createToken(
          accessPayload,
          process.env.ACCESS_SECRET,
          process.env.ACCESS_LIFE
        )
        return h.response({ success: true }).state('at', accessToken)
      }
    }

    throw Boom.unauthorized()
  }
}
