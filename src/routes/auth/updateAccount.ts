import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth
import { hashPassword, validatePassword } from '../../services/security/hashing'

interface interfaceNewPassword {
  password: string
  newPassword: string
}

interface interfaceNewEmail {
  password: string
  email: string
}

export const newPassword = {
  method: 'POST',
  path: '/newPassword',
  handler: async (request: Request, h: ResponseToolkit) => {
    // User ID
    const userId = request.auth.credentials.userId

    // Payload password
    const { password, newPassword } = request.payload as interfaceNewPassword

    // Get prisma
    const { prisma } = request.server.app

    // Check if old password match
    const user = await prisma.user.findUnique({
      where: {
        id: userId
      },
      select: {
        password: true
      }
    })

    if (!user) throw Boom.unauthorized()

    const isValid = await validatePassword(user.password, password)

    if (!isValid) throw Boom.unauthorized("Old password doesn't match")

    // It's OK so update password
    // Hash new password
    const hashedPassword = await hashPassword(newPassword)

    await prisma.user.update({
      where: {
        id: userId
      },
      data: {
        password: hashedPassword
      }
    })

    return { success: true }
  },
  options: {
    auth: 'default', // JWT
    validate: {
      payload: Joi.object({
        password: Joi.string().required().min(6),
        newPassword: Joi.string().required().min(6).max(255)
      })
    }
  }
}

export const newEmail = {
  method: 'POST',
  path: '/newEmail',
  handler: async (request: Request): Promise<{ success: boolean }> => {
    // User ID
    const userId = request.auth.credentials.userId

    // Payload password
    const { password, email } = request.payload as interfaceNewEmail

    // Get prisma
    const { prisma } = request.server.app

    // Check if old password match
    const user = await prisma.user.findUnique({
      where: {
        id: userId
      },
      select: {
        password: true
      }
    })

    if (!user) throw Boom.unauthorized()

    const isValid = await validatePassword(user.password, password)

    if (!isValid) throw Boom.unauthorized("Old password doesn't match")

    // Check if email already exists
    const isEmailTaken =
      (await prisma.user.count({
        where: {
          email: email
        }
      })) === 0
        ? false
        : true

    if (isEmailTaken) throw Boom.badRequest('This Email already exists')

    await prisma.user.update({
      where: {
        id: userId
      },
      data: {
        email: email
      }
    })

    return { success: true }
  },
  options: {
    auth: 'default', // JWT
    validate: {
      payload: Joi.object({
        password: Joi.string().required().min(6),
        email: Joi.string().required().email()
      })
    }
  }
}
