import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth
import emailSender from '../../services/email/sender'
import { hashPassword } from '../../services/security/hashing'
import { slugify } from '../../services/helpers/slugs'

/**
 * Sign up payload
 */
interface interfaceSignup {
  name: string
  email: string
  password: string
}

/**
 * Sign Up
 *
 * Basic signup for now
 * Take name, email and password
 * and create account in database.
 *
 * Leave place for 2 factor authentication
 * in future
 */
export default {
  method: 'POST',
  path: '/signup',
  handler: async (request: Request): Promise<{ success: boolean }> => {
    const req = request.payload as interfaceSignup

    const name = req.name
    const password = req.password
    const email = req.email.toLowerCase()

    const { prisma } = request.server.app

    if (name.toLowerCase() === 'mynth')
      throw Boom.badRequest("Name 'mynth' is not allowed")

    /**
     * Check if email already exists
     */
    await prisma.user
      .count({
        where: {
          email: email
        }
      })
      .then((result) => {
        if (result !== 0) throw Boom.badRequest('Email taken')
      })

    // Hash password
    const hashedPassword = await hashPassword(password)

    // Time for slugifying
    const slugBase = await slugify(name)

    // Now find slugs with same base and get last id
    // Return new id or 0 if not taken
    const slugId = await prisma.profile
      .findFirst({
        where: {
          slug_base: slugBase
        },
        orderBy: {
          slug_id: 'desc'
        },
        select: {
          slug_id: true
        }
      })
      .then((result) => (result ? result.slug_id + 1 : 0 || 0))

    // Give our slug to variable
    let slug = slugBase

    // Check if there is result and if it's there assign new id to our slug
    if (slugId) slug = `${slug}-${slugId}`

    /**
     * Create user and get created object
     */
    await prisma.user
      .create({
        data: {
          name: name,
          email: email,
          password: hashedPassword,
          profile: {
            create: {
              name: name,
              slug: slug,
              slug_base: slugBase,
              slug_id: slugId
            }
          }
        },
        include: {
          profile: true
        }
      })
      .catch((e) => {
        request.logger.info(e)
        throw Boom.internal()
      })

    // Send welcome email
    await emailSender(
      'Mynth 💚',
      email,
      'Hello! Welcome to Mynth! We are so happy 😀',
      'welcome',
      { name: name }
    )

    return { success: true }
  },
  options: {
    validate: {
      payload: Joi.object({
        name: Joi.string().required().min(3).max(60),
        email: Joi.string().required().email(),
        password: Joi.string().required().min(6).max(255)
      })
    }
  }
}
