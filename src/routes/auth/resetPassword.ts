import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'
import dayjs from 'dayjs'
import { nanoid } from 'nanoid/async'

// Mynth
import { hashPassword } from '../../services/security/hashing'

// Config
import securityConfig from '../../config/security'
import emailSender from '../../services/email/sender'

/**
 * Email
 */
interface interfaceEmail {
  email: string
}

/**
 * Password
 */
interface interfacePassword {
  password: string
}

/**
 * Issue token to reset password
 */
export const issue = {
  method: 'POST',
  path: '/resetPassword',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Get email from payload
    const { email } = request.payload as interfaceEmail
    // Prisma
    const { prisma } = request.server.app

    // Check if email in database
    const user = await prisma.user.findUnique({
      where: {
        email: email
      }
    })

    /**
     * If email address doesn't exists
     * we won't inform user about that.
     * It will improve security, there won't be
     * a way to check if email is in our database
     */

    // And if user exists, then generate and send email with token
    if (user) {
      // Generate token
      const token = await nanoid(securityConfig.resetPasswordTokenLength)

      // Set valid to date
      const validTo = dayjs()
        .add(securityConfig.resetPasswordTokenValidMinutes, 'minutes')
        .format()

      // Update database
      await prisma.user
        .update({
          where: {
            email: email
          },
          data: {
            reset_token: token,
            reset_token_valid_to: validTo
          }
        })
        .catch((e) => {
          request.logger.error(e)
          throw Boom.internal()
        })

      // Send token
      await emailSender(
        'Mynth',
        email,
        'You wanted to reset your password.',
        'resetPassword',
        { name: user.name, token: token }
      )
    }

    return { success: true }
  },
  options: {
    validate: {
      payload: Joi.object({
        email: Joi.string().required().email()
      })
    }
  }
}

/**
 * Reset password with token
 */
export const reset = {
  method: 'POST',
  path: '/resetPassword/{token}',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Get password from payload
    const { password } = request.payload as interfacePassword
    // Get token
    const token = request.params.token

    if (token.length !== securityConfig.resetPasswordTokenLength)
      throw Boom.badRequest('Token is invalid')

    // Prisma
    const { prisma } = request.server.app

    // Get user
    const user = await prisma.user.findUnique({
      where: {
        reset_token: token
      }
    })

    let isValid = false
    if (user && user.reset_token_valid_to)
      isValid = dayjs().isBefore(dayjs(user.reset_token_valid_to))

    if (!user || !isValid) throw Boom.badRequest('Token is invalid')

    // Hash password
    const hashedPassword = await hashPassword(password)

    // Continue, everything is great
    await prisma.user
      .update({
        where: {
          reset_token: token
        },
        data: {
          password: hashedPassword,
          reset_token: null,
          reset_token_valid_to: null
        }
      })
      .catch((e) => {
        request.logger.error(e)
        throw Boom.internal()
      })

    return { success: true, goto: 'https://mynth.io/signin' }
  },
  options: {
    validate: {
      params: Joi.object({
        token: Joi.string().required()
      }),
      payload: Joi.object({
        password: Joi.string().min(6).max(255)
      })
    }
  }
}
