import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'

// Mynth
export default {
  method: 'GET',
  path: '/signout',
  handler: async (request: Request, h: ResponseToolkit) => {
    return h.response({success: true}).unstate('at').unstate('rt')
  }
}
