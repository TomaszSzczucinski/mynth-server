import signin from './signin'
import signup from './signup'
import signout from './signout'
import refreshToken from './refreshToken'
import wipeAccount from './wipeAccount'
import * as updateAccount from './updateAccount'
import * as resetPassword from './resetPassword'

export default [
  signin,
  signout,
  refreshToken,
  updateAccount.newPassword,
  updateAccount.newEmail,
  resetPassword.issue,
  resetPassword.reset,
  wipeAccount
]
