import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'
import dotenv from 'dotenv'
// Mynth
import { validatePassword } from '../../services/security/hashing'
import {
  createAccessTokenPayload,
  createRefreshTokenPayload,
  createToken
} from '../../services/security/tokens'

dotenv.config()

/**
 * Sign in payload
 */
interface interfaceSignin {
  email: string
  password: string
}

export default {
  method: 'POST',
  path: '/signin',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Make sure we have our secret available
    if (
      !process.env.REFRESH_SECRET ||
      !process.env.REFRESH_LIFE ||
      !process.env.ACCESS_SECRET ||
      !process.env.ACCESS_LIFE
    )
      return Boom.internal()

    const req = request.payload as interfaceSignin
    const password = req.password
    const email = req.email.toLowerCase()

    if (password.length >= 6) {
      // Get prisma client
      const { prisma } = request.server.app
      // Get user
      const user = await prisma.user.findUnique({
        where: {
          email: email
        },
        include: {
          profile: {
            select: {
              id: true
            }
          }
        }
      })

      if (user) {
        // Compare passworrd
        const match = await validatePassword(user.password, password)

        if (match) {
          const accessPayload = await createAccessTokenPayload(
            user.id,
            user.email,
            user?.profile?.id || 0,
            user.role
          )

          const token = await createToken(
            accessPayload,
            process.env.ACCESS_SECRET,
            process.env.ACCESS_LIFE
          )

          const refreshPayload = await createRefreshTokenPayload(user.id)

          const refreshToken = await createToken(
            refreshPayload,
            process.env.REFRESH_SECRET,
            process.env.REFRESH_LIFE
          )

          return h
            .response({ success: true })
            .state('rt', refreshToken)
            .state('at', token)
        }
      }
    }

    throw Boom.badRequest('Bad data provided')
  },
  options: {
    validate: {
      payload: Joi.object({
        email: Joi.string().required().email(),
        password: Joi.string().required()
      })
    }
  }
}
