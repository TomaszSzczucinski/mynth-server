import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth
import { hashPassword, validatePassword } from '../../services/security/hashing'

interface interfaceWipeAccount {
  password: string
}

// TODO: In future we should only add flag to DB and remove account inside queue
// For now it's ok, but with more queries and users it will be hard to do it on the go
// Without small preformance loose
export default  {
  method: 'POST',
  path: '/wipeAccount',
  handler: async (request: Request, h: ResponseToolkit) => {

    // User ID
    const userId = request.auth.credentials.userId

    // Profile ID
    const profileId = request.auth.credentials.profileId

    // Payload password
    const { password } = request.payload as interfaceWipeAccount   

    // Get prisma
    const { prisma } = request.server.app
    
    // Check if old password match
    const user = await prisma.user.findUnique({
      where: {
        id: userId
      },
      select: {
        password: true
      }
    })

    if (!user) throw Boom.unauthorized()
    
    const isValid = await validatePassword(user.password, password)

    if (!isValid) throw Boom.unauthorized('Old password doesn\'t match')

    // It's OK so wipe account :(
    const query = [
      prisma.profile.delete({
        where: {
          id: profileId
        }
      }),
      prisma.user.delete({
        where: {
          id: userId
        }
      })
    ]

    const transaction = await prisma.$transaction(query)

    if (!transaction) throw Boom.internal()
    
    return { success: true }
  },
  options: {
    auth: 'default', // JWT
    validate: {
      payload: Joi.object({
        password: Joi.string().required().min(6)
      })
    }
  }
}