import health from './health'
import blog from './blog'
import profile from './profile'
import auth from './auth'
import god from './god'
import get from './get'

// Export routes
export default [...health, ...blog, ...profile, get, ...auth, ...god]
