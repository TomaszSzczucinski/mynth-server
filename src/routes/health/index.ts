import { Request, ResponseToolkit } from '@hapi/hapi'

export default [
  {
    method: 'GET',
    path: '/',
    handler: async (request: Request, h: ResponseToolkit) => {
      return { up: true }
    }
  }
]
