import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Path from 'path'
import Joi from 'joi'

// Mynth
import imageConfig from '../../config/image'
import { imageQueue, mediaRemoveQueue } from '../../services/queue'

interface interfaceImageUpload {
  image: any
}

export default [
  {
    method: 'POST',
    path: '/media/image',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      const { image } = request.payload as interfaceImageUpload

      if (!image) throw Boom.badRequest()

      image.fileName = Path.basename(image.path)

      // Add this image to queue
      await imageQueue.add({ image: image, profileId: profileId })

      return { success: true, image: image.fileName }
    },
    config: {
      auth: 'default',
      payload: {
        output: 'file',
        parse: true,
        multipart: true,
        maxBytes: imageConfig.image.maxFileSize
      }
    }
  },
  {
    method: 'GET',
    path: '/media/image',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      const images = await prisma.media.findMany({
        where: {
          profileId: profileId
        },
        select: {
          name: true,
          base: true,
          postfix: true
        },
      })

      if (!images) throw Boom.badRequest('No media found')

      return { success: true, images: images }
    },
    config: {
      auth: 'default'
    }
  },
  {
    method: 'GET',
    path: '/media/image/{imageName}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      const { imageName } = request.params

      // Get prisma client
      const { prisma } = request.server.app

      const image = await prisma.media.findUnique({
        where: {
          name_profileId: {
            name: imageName,
            profileId: profileId
          }
        },
        select: {
          name: true,
          base: true,
          postfix: true
        },
      })

      if (!image) throw Boom.badRequest('No media found')

      return { success: true, image: image }
    },
    config: {
      auth: 'default',
      validate: {
        params: Joi.object({
          imageName: Joi.string().min(3)
        })
      }
    }
  },
  {
    method: 'DELETE',
    path: '/media/image/{imageName}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      const { imageName } = request.params

      await mediaRemoveQueue.add({name: imageName, profileId: profileId})

      return { success: true }
    },
    config: {
      auth: 'default',
      validate: {
        params: Joi.object({
          imageName: Joi.string().min(3)
        })
      }
    }
  }
]
