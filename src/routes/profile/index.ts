import get from './get'
import edit from './edit'
import media from './media'
import avatar from './avatar'

export default [...get, ...edit, ...media, ...avatar]
