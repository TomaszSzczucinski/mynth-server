import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth

/**
 * Sign in payload
 */
interface interfaceEdit {
  bio?: string
  emote?: string
  technology?: number
  category?: number
}

export default [
  {
    method: 'POST',
    path: '/profile',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId
      const payload = request.payload as interfaceEdit

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      await prisma.profile.update({
        where: {
          id: profileId
        },
        data: {
          bio: payload.bio,
          emote: payload.emote
        }
      })

      return { success: true }
    },
    options: {
      auth: 'default',
      validate: {
        payload: Joi.object({
          bio: Joi.string().max(255),
          emote: Joi.string()
        })
      }
    }
  },
  {
    method: 'PUT',
    path: '/profile/technology/{technologyId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      const technologyId = Number(request.params.technologyId)

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      await prisma.profile
        .update({
          where: {
            id: profileId
          },
          data: {
            technologies: {
              connect: {
                id: technologyId
              }
            }
          }
        })
        .catch((e) => {
          throw Boom.badRequest()
        })

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'DELETE',
    path: '/profile/technology/{technologyId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      const technologyId = Number(request.params.technologyId)

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      await prisma.profile
        .update({
          where: {
            id: profileId
          },
          data: {
            technologies: {
              disconnect: {
                id: technologyId
              }
            }
          }
        })
        .catch((e) => {
          throw Boom.badRequest()
        })

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'PUT',
    path: '/profile/category/{categoryId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      const categoryId = Number(request.params.categoryId)

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      await prisma.profile
        .update({
          where: {
            id: profileId
          },
          data: {
            categories: {
              connect: {
                id: categoryId
              }
            }
          }
        })
        .catch((e) => {
          throw Boom.badRequest()
        })

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'DELETE',
    path: '/profile/category/{categoryId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      const categoryId = Number(request.params.categoryId)

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      await prisma.profile
        .update({
          where: {
            id: profileId
          },
          data: {
            categories: {
              disconnect: {
                id: categoryId
              }
            }
          }
        })
        .catch((e) => {
          throw Boom.badRequest()
        })

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  }
]
