import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth

export default [
  {
    method: 'GET',
    path: '/profile',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get ID
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      const profile = await prisma.profile.findUnique({
        where: {
          id: profileId
        },
        select: {
          name: true,
          avatar: true,
          slug: true,
          bio: true,
          emote: true,
          technologies: {
            select: {
              id: true,
              name: true,
              description: true
            }
          },
          categories: {
            select: {
              id: true,
              name: true,
              description: true
            }
          },
          snippets: true,
        }
      })

      return { success: true, profile: profile }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'GET',
    path: '/profile/likes',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get ID
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      const likes = await prisma.profile.findUnique({
        where: {
          id: profileId
        },
        select: {
          postLikes: {
            select: {
              postId: true,
            }
          }
        }
      }).then((data) => {
        return {
          postLikes: data?.postLikes.map(i => i.postId)
        }
      })

      return { success: true, likes: likes }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'GET',
    path: '/profile/{slug}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get ID
      const slug = request.params.slug

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      const profile = await prisma.profile.findUnique({
        where: {
          slug: slug
        },
        select: {
          name: true,
          avatar: true,
          slug: true,
          bio: true,
          emote: true,
          technologies: true,
          categories: true,
          snippets: true
        }
      })

      return { success: true, profile: profile }
    },
    options: {
      validate: {
        params: Joi.object({
          slug: Joi.string().min(3)
        })
      }
    }
  },
  {
    method: 'GET',
    path: '/profile/posts',
    handler: async (request: Request, h: ResponseToolkit) => {

      // Get ID
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      const posts = await prisma.post.findMany({
        where: {
          profileId: profileId
        },
        include: {
          postDraft: true,
          categories: true,
          technologies: true
        },
        orderBy: {
          id: 'desc'
        }
      })

      return { success: true, posts: posts }
    },
    config: {
      auth: 'default'
    }
  }
]
