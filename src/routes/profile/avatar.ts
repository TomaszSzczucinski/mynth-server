import { Request, ResponseToolkit } from '@hapi/hapi'

// Mynth
import { avatarQueue } from '../../services/queue'
import imageConfig from '../../config/image'

interface interfaceAavatarUpload {
  avatar: any
}

export default [
  {
    method: 'POST',
    path: '/avatar',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      const { avatar } = request.payload as interfaceAavatarUpload

      // Add this image to queue
      await avatarQueue.add(
        { avatar: avatar, profileId: profileId },
        {
          removeOnFail: true,
          removeOnComplete: true,
          attempts: 1 // 1 attempt for now
        }
      )

      return { success: true }
    },
    config: {
      auth: 'default',
      payload: {
        output: 'file',
        parse: true,
        multipart: true,
        maxBytes: imageConfig.avatar.maxFileSize
      }
    }
  },
  {
    method: 'GET',
    path: '/avatar',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get profile ID
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      const avatar = await prisma.profile
        .findUnique({
          where: {
            id: profileId
          },
          select: {
            avatar: true
          }
        })
        .then((result) => {
          return result?.avatar
        })

      return { success: true, avatar: avatar }
    },
    config: {
      auth: 'default'
    }
  }
]
