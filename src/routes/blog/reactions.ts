import { Request, ResponseToolkit } from '@hapi/hapi'

export default [
  {
    method: 'PUT',
    path: '/post/{postId}/like',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Get post slug
      const postId = Number(request.params.postId)

      // Get prisma client
      const { prisma } = request.server.app

      await prisma.$transaction([
        prisma.postLike.create({
          data: {
            profileId: profileId,
            postId: postId
          }
        }),
        prisma.post.update({
          where: {
            id: postId
          },
          data: {
            likes: {
              increment: 1
            }
          }
        })
      ]).catch(e => {
        return { success: true }
      })

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'DELETE',
    path: '/post/{postId}/like',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Get post slug
      const postId = Number(request.params.postId)

      // Get prisma client
      const { prisma } = request.server.app

      await prisma.$transaction([
        prisma.postLike.delete({
          where: {
            postId_profileId: {
              profileId: profileId,
              postId: postId
            }
          }
        }),
        prisma.post.update({
          where: {
            id: postId
          },
          data: {
            likes: {
              decrement: 1
            }
          }
        })
      ]).catch(e => e)

      return { success: true }
    },
    options: {
      auth: 'default'
    }
  }
]
