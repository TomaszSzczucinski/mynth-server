import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth

export default {
  method: 'GET',
  path: '/post/{slug}',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Get data
    const slug = request.params.slug

    const filters: any = {}

    // Get prisma client
    const { prisma } = request.server.app

    // Update profile
    const post = await prisma.post.findUnique({
      where: {
        slug: slug
      },
      select: {
        id: true,
        isPublic: true,
        title: true,
        slug: true,
        data: true,
        likes: true,
        profile: {
          select: {
            name: true,
            slug: true,
            avatar: true
          }
        },
        categories: {
          select: {
            id: true,
            name: true
          }
        },
        technologies: {
          select: {
            id: true,
            name: true
          }
        }
      }
    })

    if (!post || !post.isPublic)
      throw Boom.badRequest("Ooops. I can't find that post")

    return { success: true, post: post }
  },
  options: {
    validate: {
      params: Joi.object({
        slug: Joi.string()
      })
    }
  }
}
