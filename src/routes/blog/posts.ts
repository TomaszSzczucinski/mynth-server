import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

// Mynth
interface interfacePosts {
  categories?: string
  technologies?: string
  pLangs?: string
  tags?: string
  dateFrom?: Date
  dateTo?: Date
  max?: number
  author?: string
}

export default {
  method: 'GET',
  path: '/posts',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Get data
    const payload = request.query as interfacePosts

    let filters: any = {}

    if (payload.categories)
      filters = {
        ...filters,
        categories: {
          some: { id: { in: payload.categories.split('A').map(Number) } }
        }
      }

    if (payload.technologies)
      filters = {
        ...filters,
        technologies: {
          some: { id: { in: payload.technologies.split('A').map(Number) } }
        }
      }

    if (payload.author)
      filters = {
        ...filters,
        profile: {
          slug: payload.author
        }
      }

    // Get prisma client
    const { prisma } = request.server.app

    // Update profile
    const post = await prisma.post.findMany({
      where: {
        isPublic: true,
        ...filters
      },
      select: {
        id: true,
        createdAt: true,
        title: true,
        slug: true,
        hero: true,
        likes: true,
        profile: {
          select: {
            name: true,
            slug: true,
            avatar: true
          }
        },
        categories: {
          select: {
            id: true,
            name: true
          }
        },
        technologies: {
          select: {
            id: true,
            name: true
          }
        }
      },
      orderBy: [
        {
          createdAt: 'desc'
        }
      ],
      take: payload.max ? payload.max : undefined
    })

    return { success: true, posts: post }
  },
  options: {
    validate: {
      query: Joi.object({
        categories: Joi.string().min(1).trim(),
        technologies: Joi.string().min(1),
        pLangs: Joi.string().min(1),
        tags: Joi.string().min(1),
        dateFrom: Joi.date(),
        dateTo: Joi.date(),
        max: Joi.number().min(1),
        author: Joi.string().max(255)
      })
    }
  }
}
