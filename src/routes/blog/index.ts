import post from './post'
import posts from './posts'
import postEditor from './postEditor'
import reactions from './reactions'

export default [
  post,
  posts,
  ...reactions,
  ...postEditor
]
