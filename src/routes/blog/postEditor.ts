import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'
import sanitizeHtml from 'sanitize-html'

import { nanoid } from 'nanoid/async'
import { slugify } from '../../services/helpers/slugs'

// Mynth
interface interfaceEditPost {
  data: {
    title: string
    categories?: number[]
    technologies?: number[]
    hero?: {
      url: string
      files: string[]
    }
    blocks: [
      {
        type: string
        content?: string
        url?: string
        lang?: string
        files?: string[]
      }
    ]
  }
}

export default [
  {
    method: 'GET',
    path: '/draft/{draftId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Post ID
      const draftId = Number(request.params.draftId)

      // Get prisma client
      const { prisma } = request.server.app

      const postDraft = await prisma.postDraft.findUnique({
        where: {
          id_profileId: {
            id: draftId,
            profileId: profileId
          }
        }
      })

      if (!postDraft)
        throw Boom.notFound("Post doesn't exists or you don't have access")

      return { success: true, post: postDraft }
    },
    options: {
      auth: 'default',
      validate: {
        params: Joi.object({
          draftId: Joi.number()
        })
      }
    }
  },
  {
    method: 'POST',
    path: '/post',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Get prisma client
      const { prisma } = request.server.app

      // Update profile
      const post = await prisma.post.create({
        data: {
          title: '',
          data: {},
          isPublic: false,
          profileId: profileId,
          postDraft: {
            create: {
              profile: {
                connect: {
                  id: profileId
                }
              },
              data: {
                title: 'Hello World',
                blocks: []
              }
            }
          }
        },
        include: {
          postDraft: true
        }
      })

      // Internal if post draft wasn't created
      if (!post.postDraft) throw Boom.internal()

      return { success: true, post: post.postDraft.id }
    },
    options: {
      auth: 'default'
    }
  },
  {
    method: 'PUT',
    path: '/post/{draftId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Post ID
      const draftId = Number(request.params.draftId)

      // Payload
      const { data } = request.payload as interfaceEditPost

      // Get prisma client
      const { prisma } = request.server.app

      data.title = sanitizeHtml(data.title)

      for (let i = 0; i < data.blocks.length; i++) {
        if (data.blocks[i].type !== 'code')
          data.blocks[i].content = sanitizeHtml(data.blocks[i].content || '')
      }

      const postDraft = await prisma.postDraft.update({
        where: {
          id_profileId: {
            id: draftId,
            profileId: profileId
          }
        },
        data: {
          data,
          isPublic: false
        },
        include: {
          post: true
        }
      })

      return { success: true, post: postDraft }
    },
    options: {
      auth: 'default',
      validate: {
        params: Joi.object({
          draftId: Joi.number()
        }),
        payload: Joi.object({
          data: Joi.object({
            title: Joi.string().max(90).required(),
            categories: Joi.array().items(Joi.number()).max(3),
            technologies: Joi.array().items(Joi.number()).max(3),
            hero: Joi.object({
              url: Joi.string().min(3).max(150).required(),
              files: Joi.array().items(Joi.string().max(15))
            }),
            blocks: Joi.array()
              .items({
                type: Joi.string().max(30).required(),
                content: Joi.string().allow(''),
                url: Joi.string().uri(),
                lang: Joi.string().max(15),
                files: Joi.array().items(Joi.string().min(0))
              })
              .required()
          })
        })
      }
    }
  },
  {
    method: 'POST',
    path: '/post/{draftId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Post ID
      const draftId = Number(request.params.draftId)

      // Get prisma client
      const { prisma } = request.server.app

      const postDraft = await prisma.postDraft.findUnique({
        where: {
          id_profileId: {
            id: draftId,
            profileId: profileId
          }
        }
      })

      // If no post draft
      if (!postDraft) throw Boom.badRequest()

      const data: any = postDraft.data

      // We need to do some data veryfing
      if (!data.title) throw Boom.badRequest('Please add title to post')
      if (!data.categories || data.categories.length < 1)
        throw Boom.badRequest('Please add minimum 1 category')

      // Create slug
      const slug = await slugify(data.title).then(
        async (s) => `${s}-${await nanoid(6)}`
      )

      console.log(
        data.categories.map((i: number) => {
          return { id: i }
        })
      )

      const post = await prisma.post
        .update({
          where: {
            id: postDraft.postId
          },
          data: {
            isPublic: true,
            slug: slug,
            hero: data.hero,
            title: data.title,
            technologies: data.technologies
              ? {
                  set: data.technologies.map((i: number) => {
                    return { id: i }
                  })
                }
              : undefined,
            categories: data.categories
              ? {
                  set: data.categories.map((i: number) => {
                    return { id: i }
                  })
                }
              : undefined,
            data: data,
            postDraft: {
              update: {
                isPublic: true
              }
            }
          }
        })
        .catch((e) => {
          throw Boom.badRequest()
        })

      return { success: true, post: post }
    },
    options: {
      auth: 'default',
      validate: {
        params: Joi.object({
          draftId: Joi.number()
        })
      }
    }
  },
  {
    method: 'DELETE',
    path: '/post/{postId}',
    handler: async (request: Request, h: ResponseToolkit) => {
      // Get data
      const profileId = request.auth.credentials.profileId

      // Post ID
      const postId = Number(request.params.postId)

      // Delete post?
      const doDelete = request.query['delete']

      // Get prisma client
      const { prisma } = request.server.app

      if (doDelete) {
        // Delete post
        await prisma.$transaction([
          prisma.postLike.deleteMany({
            where: {
              postId: postId
            }
          }),
          prisma.postDraft.deleteMany({
            where: {
              postId: postId,
              profileId: profileId
            }
          }),
          prisma.post.deleteMany({
            where: {
              id: postId,
              profileId: profileId
            }
          })
        ])
      } else {
        // Unpublish post
        await prisma.post.update({
          where: {
            id: postId
          },
          data: {
            isPublic: false,
            postDraft: {
              update: {
                isPublic: false
              }
            }
          }
        })
      }

      return { success: true, deleted: doDelete }
    },
    options: {
      auth: 'default',
      validate: {
        params: Joi.object({
          postId: Joi.number()
        }),
        query: Joi.object({
          delete: Joi.boolean()
        })
      }
    }
  }
]
