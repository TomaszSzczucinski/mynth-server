import { Request, ResponseToolkit } from '@hapi/hapi'
import Boom from '@hapi/boom'
import Joi from 'joi'

interface interfaceQuery {
  categories?: boolean
  technologies?: boolean
}

// Mynth
export default {
  method: 'GET',
  path: '/get',
  handler: async (request: Request, h: ResponseToolkit) => {
    // Get query
    const query = request.query as interfaceQuery

    // Get prisma client
    const { prisma } = request.server.app

    const transaction = []
    const data: {
      categories?: any
      technologies?: any
    } = {}

    if (query.categories)
      data.categories = await prisma.category
        .findMany({
          include: {
            posts: {
              where: {
                isPublic: true
              },
              select: {
                id: true
              }
            }
          }
        })
        .then((categories: any) => {
          return categories.map((i: any) => {
            i.posts = i.posts.length
            return i
          })
        })

    if (query.technologies)
      data.technologies = await prisma.technology.findMany({
        include: {
          posts: {
            where: {
              isPublic: true
            },
            select: {
              id: true
            }
          }
        }
      }).then((technologies: any) => {
        return technologies.map((i: any) => {
          i.posts = i.posts.length
          return i
        })
      })

    return { success: true, ...data }
  },
  config: {
    validate: {
      query: Joi.object({
        categories: Joi.boolean(),
        technologies: Joi.boolean()
      })
    }
  }
}
