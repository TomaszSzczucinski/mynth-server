// Hapi
import Hapi from '@hapi/hapi'

// Public plugins
import dotenv from 'dotenv'

import schemeToken from './schemes/scheme.token'

// Routes
import routes from './routes'

// Local plugins
import plugins from './plugins'

dotenv.config()

const isProduction = process.env.NODE_ENV === 'production'

const server: Hapi.Server = Hapi.server({
  port: process.env.PORT || 3000,
  host: process.env.HOST || '0.0.0.0',
  routes: {
    cors: {
      credentials: true
    }
  }
})

export async function createServer(): Promise<Hapi.Server> {
  // Register the logger
  await server.register({
    plugin: require('hapi-pino'),
    options: {
      logEvents:
        process.env.CI === 'true' || process.env.TEST === 'true'
          ? false
          : undefined,
      prettyPrint: process.env.NODE_ENV !== 'production',
      // Redact Authorization headers, see https://getpino.io/#/docs/redaction
      redact: ['req.headers.authorization']
    }
  })

  // server.register({
  //   plugin: require('disinfect'),
  //   options: {
  //     disinfectQuery: true,
  //     disinfectParams: true,
  //     disinfectPayload: true
  //   }
  // })

  server.state('at', {
    ttl: 1000 * 60 * 15,
    isSecure: false,
    isHttpOnly: true,
    encoding: 'base64json',
    clearInvalid: true,
    strictHeader: true
  })

  server.state('rt', {
    ttl: 1000 * 60 * 60 * 24 * 30,
    isSecure: false,
    isHttpOnly: true,
    encoding: 'base64json',
    clearInvalid: true,
    strictHeader: true,
    path: '/refreshToken'
  })

  // Register auth strategy
  server.auth.scheme('jwt', schemeToken)
  server.auth.strategy('default', 'jwt', { role: 10 })
  server.auth.strategy('god', 'jwt', { role: 1000 })

  // Register plugins
  await server.register(plugins)

  // Register routes
  server.route(routes)

  // Initialize consumers
  // Extract into separate thread/processes in future

  // Email consumer
  import('./services/queue/emailConsumer')
  import('./services/queue/avatarConsumer')
  import('./services/queue/imageConsumer')
  import('./services/queue/mediaRemoveConsumer')
  import('./services/queue/imageOnCompleted')
  import('./services/queue/imageOnFailed')

  // Initialize server
  await server.initialize()

  return server
}

export async function startServer(server: Hapi.Server): Promise<Hapi.Server> {
  await server.start()
  server.log('info', `Server running on ${server.info.uri}`)
  return server
}

process.on('unhandledRejection', (err) => {
  console.log(err)
  process.exit(1)
})
