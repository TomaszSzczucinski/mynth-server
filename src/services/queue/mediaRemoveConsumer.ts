import imageConfig from '../../config/image'
import fs from 'fs'
import sharp from 'sharp'
import { PrismaClient } from '@prisma/client'

// Get prisma client
const prisma = new PrismaClient()

// Mynth
import { mediaRemoveQueue } from '.'

export default mediaRemoveQueue.process(async (job) => {
  const { name, profileId } = job.data

  console.log(
    `[MEDIA_REMOVE_QUEUE] ==> Media remove consumer queue (${job.id})`
  )
  console.log(`[MEDIA_REMOVE_QUEUE] ==> Attempts made: ${job.attemptsMade}`)

  console.log(`[MEDIA_REMOVE_QUEUE] ==> File name: ${name}`)
  
  // Create array of sharp resize processes and final urls
  for (let i = 0; i < imageConfig.image.configs.length; i++) {
    const config = imageConfig.image.configs[i]
    const path = `${imageConfig.path}${name}${config.postfix}.${config.ext}`

    // Check if file exists
    fs.access(path, (err) => {
      if (err) {
        console.log(`[MEDIA_REMOVE_QUEUE] ==> File does not exists: ${name}`)
      } else {
        console.log(`[MEDIA_REMOVE_QUEUE] ==> Removing file: ${name}`)
        fs.unlink(path, (err) => {
          if (!err) console.log(`[MEDIA_REMOVE_QUEUE] ==> Successfully removed: ${name}`)
        })
      }
    })
  }

  await prisma.media.delete({
    where: {
      name_profileId: {
        name: name,
        profileId: profileId
      }
    }
  }).catch(() => console.log(`[MEDIA_REMOVE_QUEUE] ==> No entry in DB: ${name}`))
})
