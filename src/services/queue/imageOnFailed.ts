import fs from 'fs'

// Mynth
import { imageQueue } from '.'

export default imageQueue.on('failed', async (job) => {
  const { image } = job.data
  console.log(`[IMAGE_Q] ==> Failed job: ${job.id}`)
  console.log(`[IMAGE_Q] ==> Reason: ${job.failedReason}`)
  
  if (job.attemptsMade === job.opts.attempts)
  {
    console.log(`[IMAGE_Q] ==> Removing file ${image.path}`)

    // Remove tmp file
    fs.unlink(image.path, async () => {
      console.log(`[IMAGE_Q] ==> Successfully removed file ${image.path}`)
    })
  }
})


