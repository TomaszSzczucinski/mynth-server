import Path from 'path'
import fs from 'fs'
import Handlebars from 'handlebars'
import { htmlToText } from 'html-to-text'

import emailConfig from '../../config/email'
import { emailQueue } from '.'
import { getTransporter } from '../email/transporter'

export default emailQueue.process(async (job) => {
  const { from, to, subject, template, args } = job.data

  const templatePath = Path.resolve(__dirname, '..', '..', '..', 'emailTemplates', `${template}.html`)

  const content = fs.readFileSync(templatePath, 'utf-8')

  const hbTemplate = Handlebars.compile(content)
  const html = hbTemplate(args)
  const text = htmlToText(html)

  const transporter = await getTransporter()
  
  await transporter.sendMail({
    from: `${from} <${emailConfig.from}>`,
    to: to, // list of receivers
    subject: subject, // Subject line
    text: text, // plain text body
    html: html // html body
  }).catch(async (e) => {
    return e
  })
})
