import imageConfig from '../../config/image'
import sharp from 'sharp'
import { PrismaClient } from '@prisma/client'

// Get prisma client
const prisma = new PrismaClient()

// Mynth
import { imageQueue } from '.'

export default imageQueue.process(async (job) => {
  const { image, profileId } = job.data

  console.log(`[IMAGE_Q] ==> Image consumer queue (${job.id})`)
  console.log(`[IMAGE_Q] ==> Attempts made: ${job.attemptsMade}`)

  // Unique file name from actual file
  const fileName = image.fileName

  console.log(`[IMAGE_Q] ==> File name: ${fileName}`)

  // Placeholders for processes and urls
  const processes = []
  const urls: string[] = []

  // Create array of sharp resize processes and final urls
  for (let i = 0; i < imageConfig.image.configs.length; i++) {
    const config = imageConfig.image.configs[i]
    const process = sharp(image.path)
      .rotate()
      .resize(config.width, config.height)
      .toFile(`${imageConfig.path}${fileName}${config.postfix}.${config.ext}`)
    processes.push(process)
    urls.push(`${imageConfig.url}${fileName}${config.postfix}.${config.ext}`)
  }

  // Run sharp processes
  await Promise.all(processes).then(async () => {
    await prisma.media.create({
      data: {
        profileId: profileId,
        base: imageConfig.url,
        name: fileName,
        postfix: imageConfig.image.configs.map(i => `${i.postfix}.${i.ext}`)
      }
    })
  })
})
