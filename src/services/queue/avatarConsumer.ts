import Path from 'path'
import fs from 'fs'
import imageConfig from '../../config/image'
import sharp from 'sharp'
import { PrismaClient } from '@prisma/client'

// Get prisma client
const prisma = new PrismaClient()

// Mynth
import { avatarQueue } from '.'

export default avatarQueue.process(async (job) => {
  const { avatar, profileId } = job.data

  // Unique file name from actual file
  const fileName = Path.basename(avatar.path)

  // We need to retrieve old path to avatar
  // to delete it after change
  const oldAvatarFile = await prisma.profile
    .findUnique({
      where: {
        id: profileId
      },
      select: {
        avatar: true
      }
    })
    .then((res) => res && res.avatar ? Path.basename(res.avatar).split('.')[0] : null)

  const oldPaths: string[] = []

  if (oldAvatarFile) {
    for (let i = 0; i < imageConfig.avatar.configs.length; i++) {
      const config = imageConfig.avatar.configs[i]
      oldPaths.push(
        `${imageConfig.path}${oldAvatarFile}${config.postfix}.${config.ext}`
      )
    }
  }

  // Placeholders for processes and urls
  const processes = []
  const urls: string[] = []

  // Create array of sharp resize processes and final urls
  for (let i = 0; i < imageConfig.avatar.configs.length; i++) {
    const config = imageConfig.avatar.configs[i]
    const process = sharp(avatar.path)
      .rotate()
      .resize(config.width, config.height)
      .toFile(`${imageConfig.path}${fileName}${config.postfix}.${config.ext}`)
    processes.push(process)
    urls.push(`${imageConfig.url}${fileName}${config.postfix}.${config.ext}`)
  }

  // Run sharp processes
  await Promise.all(processes)
    .then(async () => {
      await prisma.profile
        .update({
          where: {
            id: profileId
          },
          data: {
            avatar: urls[0]
          }
        })
        .then(() => {
          if (oldAvatarFile) {
            oldPaths.forEach((path) => {
              try {
                console.log(`Unlinking ${path}...`)
                fs.unlinkSync(path)
              } catch (e) {
                console.log(e)
              }
            })
          }
        })
    })
    .finally(() => {
      // Remove tmp file
      fs.unlinkSync(avatar.path)
    })
})
