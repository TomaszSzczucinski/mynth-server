import fs from 'fs'

// Mynth
import { imageQueue } from '.'

export default imageQueue.on('completed', async (job) => {
  const { image } = job.data

  console.log(`[IMAGE_Q] ==> Image queue completed (${job.id})`)
  console.log(`[IMAGE_Q] ==> Removing file ${image.path}`)

  // Remove tmp file
  fs.unlink(image.path, async () => {
    console.log(`[IMAGE_Q] ==> Successfully removed file ${image.path}`)
  })
})


