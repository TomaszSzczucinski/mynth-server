import Queue from 'bull'
import Redis from 'ioredis'
import dotenv from 'dotenv'
import queueConfig from '../../config/queue'
import fs from 'fs'
dotenv.config()

const client = new Redis({
  port: Number(process.env.REDIS_PORT),
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_DBID)
})

const subscriber = new Redis({
  port: Number(process.env.REDIS_PORT),
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_DBID)
})

const opts = {
  createClient: (type: string) => {
    switch (type) {
      case 'client':
        return client
      case 'subscriber':
        return subscriber
      case 'bclient':
        return new Redis({
          port: Number(process.env.REDIS_PORT),
          password: process.env.REDIS_PASS,
          db: Number(process.env.REDIS_DBID)
        })
      default:
        throw new Error('Unexpected connection type: ' + type)
    }
  }
}

export const emailQueue = new Queue('EMAIL_QUEUE', {
  ...opts,
  limiter: {
    max: queueConfig.emailMax,
    duration: queueConfig.emailDuration
  }
})

export const avatarQueue = new Queue('AVATAR_QUEUE', {
  ...opts,
  limiter: {
    max: queueConfig.avatarMax,
    duration: queueConfig.avatarDuration
  }
})

export const imageQueue = new Queue('IMAGE_QUEUE', {
  ...opts,
  defaultJobOptions: {
    attempts: queueConfig.imageAttempts,
    backoff: queueConfig.imageAttemptsDelay,
    removeOnComplete: true,
    removeOnFail: true
  },
  limiter: {
    max: queueConfig.imageMax,
    duration: queueConfig.imageDuration
  }
})

export const mediaRemoveQueue = new Queue('MEDIA_REMOVE_QUEUE', {
  ...opts,
  defaultJobOptions: {
    attempts: queueConfig.removeAttempts,
    backoff: queueConfig.removeAttemptsDelay,
    removeOnComplete: true,
    removeOnFail: true
  },
  limiter: {
    max: queueConfig.removeMax,
    duration: queueConfig.removeDuration
  }
})
