/**
 * Slugifies string into url friendly format
 * @param toSlugify String to be slugified
 */
export const slugify = async (toSlugify: string) => {
  // Replace and lowercase
  toSlugify = toSlugify.replace(/^\s+|\s+$/g, '').toLowerCase()

  // Remove accents
  const from =
    'ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽąáäâàãåćčçćďęéěëèêẽĕȇíìîïńňñóóöòôõøðřŕšťúůüùûýÿżźžþÞĐđßÆa·/_,:;'
  const to =
    'AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaaaccccdeeeeeeeeeiiiinnnoooooooorrstuuuuuyyżźzbBDdBAa------'

  for (let i = 0, l = from.length; i < l; i++) {
    toSlugify = toSlugify.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))
  }

  // Remove invalid chars
  toSlugify = toSlugify
    .replace(/[^a-z0-9 -]/g, '')
    // Collapse whitespace and replace by -
    .replace(/\s+/g, '-')
    // Collapse dashes
    .replace(/-+/g, '-')

  return toSlugify
}
