/**
 * Returns redis key for token ID
 * @param id Refresh token ID
 */
export const refreshTokenKey = async (id: number) => {
  return "rtk_" + String(id)
}