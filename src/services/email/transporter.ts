/**
 * Nodemailer transport ready to go for mynth
 */
import nodemailer from 'nodemailer'

export const getTransporter = async (): Promise<nodemailer.Transporter> => {
  
  const transporter = nodemailer.createTransport({
    host: 'localhost',
    port: 1025,
    auth: {
      user: 'project.1',
      pass: 'secret.1'
    }
  })

  return transporter
}
