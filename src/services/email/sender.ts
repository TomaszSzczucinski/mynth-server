import { emailQueue } from '../queue'

/**
 * Send email helper
 * @param from Email from
 * @param to Email receiver
 * @param subject Email subject
 * @param template Email template to use (filename)
 * @param args Templating options
 */
const emailSender =  async (from: string, to: string, subject: string, template: string, args: any) => {
  await emailQueue
    .add(
      {
        from: from,
        to: to,
        subject: subject,
        template: template,
        args: args
      },
      { removeOnComplete: true, attempts: 30, backoff: 5000 }
    )
}

export default emailSender