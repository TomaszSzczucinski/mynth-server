import { Profile, User } from '@prisma/client'
import jwt from 'jsonwebtoken'

/**
 * Create payload for access token
 * @param user User object from DB
 * @param profileId Profile ID
 */
export const createAccessTokenPayload = async (
  userId: number, email: string, profileId: number, role: number
): Promise<{ i: number; e: string; p: number, r: number }> => {
  return {
    i: userId,
    e: email,
    p: profileId,
    r: role
  }
}

/**
 * Create payload for refresh token
 * @param referenceId Reference ID
 */
export const createRefreshTokenPayload = async (
  userId: number
): Promise<{ i: number }> => {
  return {
    i: userId
  }
}

/**
 * Create and sign JWT Token
 * @param payload Payload to save inside token
 * @param secret Secret used to secure token
 */
export const createToken = async (
  payload: any,
  secret: string,
  expiresIn: string
): Promise<string> => {
  const token = jwt.sign(payload, secret, {
    algorithm: 'HS384',
    expiresIn: expiresIn
  })
  return token
}

/**
 * Validates and return token payload
 * @param token JWT Token
 * @param secret Token secret
 */
export const validateToken = async (
  token: string,
  secret: string
): Promise<any> => {
  // jwt validate
  const payload = jwt.verify(token, secret)
  return payload
}
