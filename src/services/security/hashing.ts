import * as argon2 from 'argon2'

/**
 * Hash password
 * @param password Plain text password
 * @returns Hashed password
 */
export const hashPassword = async (password: string): Promise<string> => {
  return await argon2.hash(password, {
    type: argon2.argon2id,
    saltLength: 32,
    timeCost: 5
  })
}

/**
 * Validate hashed password
 * @param hash Hashed Password
 * @param password Password
 */
export const validatePassword = async (hash: string, password: string): Promise<boolean> => {
  return await argon2.verify(hash, password)
}
