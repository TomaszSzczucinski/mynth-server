export default {
  emailMax: 30,
  emailDuration: 2000,
  avatarMax: 10,
  avatarDuration: 5000,
  imageMax: 30,
  imageDuration: 5000,
  imageAttempts: 3,
  imageAttemptsDelay: 3000, // 3 seconds
  removeMax: 5,
  removeDuration: 5000,
  removeAttempts: 3,
  removeAttemptsDelay: 3000 // 3 seconds
}
