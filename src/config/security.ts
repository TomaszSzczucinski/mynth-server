/**
 * Security common configuration
 * 
 * Passwords, etc. are in .env file for better security
 */
export default {
  resetPasswordTokenLength: 45,
  resetPasswordTokenValidMinutes: 15
}