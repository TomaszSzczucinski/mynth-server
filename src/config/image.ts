/**
 * Email configuration
 */
export default {
  path: '/var/www/m1.mynth.io/mmv1',
  url: 'https://m1.mynth.io/mmv1/',
  avatar: {
    maxFileSize: 10485760, // 10 MB Thus will be converted
    configs: [
      {
        width: 400,
        height: 400,
        ext: 'webp',
        postfix: ''
      },
      {
        width: 50,
        height: 50,
        ext: 'png',
        postfix: '-tiny'
      }
    ]
  },
  image: {
    maxFileSize: 2 * 10485760, // 20MB
    configs: [
      {
        width: 2400,
        height: null,
        ext: 'webp',
        postfix: ''
      }
    ]
  }
}
